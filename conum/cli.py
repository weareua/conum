from sqlalchemy.exc import DatabaseError
from conum import app, db
from conum.services import UserService, CounterService, RecordService


@app.cli.command()
def initdb():
    print('Starting db initialization')
    try:
        from conum import models
        db.create_all()
        print('Db was successfully initialized')
    except DatabaseError:
        print('Db initialization did fail')
        raise


@app.cli.command()
def filldb():
    print('Filling db up with data')
    try:
        print('Create user')
        user = UserService.create_user('weareua@gmail.com', 'qwerty')
        print('Create counters')
        CounterService.create_counter('brave', 'watt', 'blue', user)
        CounterService.create_counter('heart', 'bit', 'red', user)
        print('Create records')
        RecordService.create_record('10', 1)
        RecordService.create_record('30', 1)
        RecordService.create_record('65', 2)
        print('Db was filled')
    except:
        raise
