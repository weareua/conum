from datetime import datetime
from conum import db


class Counter(db.Model):
    __tablename__ = 'counters'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=False)
    dimension = db.Column(db.String(15), unique=False)
    color = db.Column(db.String(10), unique=False)
    creation_date = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', backref=db.backref(
        'counters', lazy="dynamic"))

    def __init__(self, title, dimension, color, user):
        self.title = title
        self.dimension = dimension
        self.color = color
        self.user = user
        self.creation_date = datetime.utcnow()


class Record(db.Model):
    __tablename__ = 'records'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Integer, unique=False)
    creation_date = db.Column(db.DateTime)
    counter_id = db.Column(db.Integer, db.ForeignKey('counters.id'))
    counter = db.relationship(
        'Counter', backref=db.backref('records', lazy="dynamic"))

    def __init__(self, value, counter):
        self.value = value
        self.counter = counter
        self.creation_date = datetime.utcnow()


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(32), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    reset_hash = db.Column(db.String(128))
    registration_date = db.Column(db.DateTime)
    reset_date = db.Column(db.DateTime)

    def __init__(self, email):
        self.email = email
        self.registration_date = datetime.utcnow()
