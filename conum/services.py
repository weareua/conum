from datetime import datetime, timedelta
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from conum import app
from conum.models import Record, User, Counter
from conum.utils import DbUtils


class UserService:

    @staticmethod
    def get_user_by_token(token):
        try:
            return UserService.verify_auth_token(token)
        except AttributeError:
            return False

    @staticmethod
    def get_user_by_email(email):
        return User.query.filter_by(email=email).first()

    @staticmethod
    def get_user_by_id(id):
        return User.query.get(id)

    @staticmethod
    def create_user(email, password):
        user = User(email=email)
        user.password_hash = pwd_context.encrypt(password)
        DbUtils.commit_changes(user)
        return user

    @staticmethod
    def hash_secret(email, secret_number):
        user = UserService.get_user_by_email(email=email)
        user.reset_hash = pwd_context.encrypt(secret_number)
        user.reset_date = datetime.utcnow()
        DbUtils.commit_changes(user)
        return True

    @staticmethod
    def hash_password(email, password):
        user = UserService.get_user_by_email(email=email)
        user.password_hash = pwd_context.encrypt(password)
        DbUtils.commit_changes(user)
        return True

    @staticmethod
    def verify_password(email, password):
        user = UserService.get_user_by_email(email=email)
        return pwd_context.verify(password, user.password_hash)

    @staticmethod
    def verify_secret(email, secret_number):
        user = UserService.get_user_by_email(email=email)
        return pwd_context.verify(secret_number, user.reset_hash)

    @staticmethod
    def approve_secret(email):
        user = UserService.get_user_by_email(email=email)
        user.reset_hash = "OK"
        DbUtils.commit_changes(user)
        return True

    @staticmethod
    def secret_status(email):
        user = UserService.get_user_by_email(email=email)
        if (user.reset_date + timedelta(
                hours=app.config['RECOVERY_HOURS_WINDOW'])) < datetime.utcnow():
            return False, "OLD"
        elif user.reset_hash == "OK":
            return True, "OK"
        return False, ""

    @staticmethod
    def generate_auth_token(email, expiration=900):
        user = UserService.get_user_by_email(email=email)
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': user.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = UserService.get_user_by_id(data['id'])
        return user

    @staticmethod
    def delete_user(email):
        UserService.get_user_by_email(email).delete()
        DbUtils.commit_changes()
        return True


class CounterService:

    @staticmethod
    def get_counter_by_id(id):
        return Counter.query.get(id)

    @staticmethod
    def get_all_counters():
        return Counter.query.all()

    @staticmethod
    def create_counter(title, dimension, color, user):
        counter = Counter(
            title=title, dimension=dimension, color=color, user=user)
        DbUtils.commit_changes(counter)
        return counter

    @staticmethod
    def update_counter(id, title=None, dimension=None, color=None):
        counter = CounterService.get_counter_by_id(id)
        if title:
            counter.title = title
        if dimension:
            counter.dimension = dimension
        if color:
            counter.color = color
        DbUtils.commit_changes(counter)
        return counter

    @staticmethod
    def delete_counter(id):
        Counter.query.filter(Counter.id == id).delete()
        DbUtils.commit_changes()
        return True

    @staticmethod
    def check_owner(counter_id, email):
        try:
            counter = CounterService.get_counter_by_id(counter_id)
            user = UserService.get_user_by_email(email)
            if counter.user == user:
                return True
        except AttributeError:
            return False


class RecordService:

    @staticmethod
    def get_record_by_id(id):
        return Record.query.get(id)

    @staticmethod
    def get_by_counter_id(id, page=1):
        records_query = Record.query.filter(Record.counter_id == id).paginate(
            page, app.config['RECORDS_PER_PAGE'], False)
        return records_query.items

    @staticmethod
    def create_record(value, counter_id):
        counter = CounterService.get_counter_by_id(counter_id)
        record = Record(value=value, counter=counter)
        DbUtils.commit_changes(record)
        return record

    @staticmethod
    def update_record(id, value):
        record = RecordService.get_record_by_id(id)
        record.update(value=value)
        DbUtils.commit_changes(record)
        return record

    @staticmethod
    def delete_record(id):
        Record.query.filter(Record.id == id).delete()
        DbUtils.commit_changes()
        return True
