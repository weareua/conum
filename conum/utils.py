import re
from datetime import datetime
from conum import db


class DbUtils:

    @staticmethod
    def commit_changes(object=None):
        if object:
            db.session.add(object)
        db.session.commit()


class EmailUtils:

    @staticmethod
    def check_email(email):
        pattern = re.compile(r"\"?([-a-zA-Z0-9._+`?{}]+@\w+\.\w+)\"?")
        if re.match(pattern, email):
            return True
        return False


class SerializeUtils:

    @staticmethod
    def serialize_date(date):
        if isinstance(date, datetime):
            serialized = date.strftime("%d/%m/%Y %H:%M")
            return serialized
        raise TypeError("Type is not serializable")
