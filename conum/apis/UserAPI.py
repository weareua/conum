from flask import jsonify, abort, make_response, url_for
from flask_mail import Message
from flask_restful import Resource
from conum import auth, parser, mail
from conum.utils import EmailUtils
from conum.services import UserService


class UserAPI(Resource):
    parser.add_argument('email')
    parser.add_argument('password')

    def put(self):
        args = parser.parse_args()
        email = args['email']
        password = args['password']
        if email is None or password is None:
            abort(400)  # Missing arguments
        elif EmailUtils.check_email(email) is False:
            abort(403)  # Wrong Email
        elif UserService.get_user_by_email(email) is not None:
            abort(400)  # Existing user
        user = UserService.create_user(email, password)
        # Send registration email
        msg = Message("Welcome to conum",
                      recipients=[user.email])
        msg.html = "Hi there! conum is the counters data storage platform " +\
            "that will help you to monitor your house resources consumption."
        mail.send(msg)
        return make_response(
            jsonify({'email': user.email}), 201,
            {'Location': url_for('user', id=user.id, _external=True)})

    @auth.login_required
    def get(self, id):
        user = UserService.get_user_by_id(id)
        if not user:
            abort(400)
        return ({'email': user.email})
