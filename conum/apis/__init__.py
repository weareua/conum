from flask import g, make_response, jsonify
from conum import app, auth
from conum.services import UserService


@auth.verify_password
def verify_password(email_or_token, password):
    # First try to authenticate by token
    user = UserService.get_user_by_token(email_or_token)
    if not user:
        # Try to authenticate with email/password
        user = UserService.get_user_by_email(email_or_token)
        if not user or not UserService.verify_password(
                email_or_token, password):
            return False
    g.user = user
    return True


@auth.error_handler
def unauthorized():
    # return 403 instead of 401 to prevent browsers from displaying the default
    # auth dialog
    return make_response(
        jsonify({'status': 'ERROR', 'message': 'Unauthorized access'}), 403)


@app.errorhandler(400)
def bad_request(error):
    return make_response(
        jsonify({'status': 'ERROR', 'message': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(
        jsonify({'status': 'ERROR', 'message': 'Not found'}), 404)
