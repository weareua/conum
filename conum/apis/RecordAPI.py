from flask import jsonify, abort, url_for, make_response
from flask_restful import Resource
from conum import auth, parser
from conum.services import CounterService, RecordService
from conum.utils import SerializeUtils


class RecordAPI(Resource):
    decorators = [auth.login_required]

    parser.add_argument('id')
    parser.add_argument('value')
    parser.add_argument('counter_id')
    parser.add_argument('email')

    def put(self):
        args = parser.parse_args()
        value = args['value']
        counter_id = args['counter_id']
        email = args['email']
        if value is None or counter_id is None or email is None:
            abort(400)  # Missing arguments
        if not CounterService.check_owner(counter_id, email):
            abort(403)  # User isn't the counter's owner
        record = RecordService.create_record(value, counter_id)
        serialized_date = SerializeUtils.serialize_date(record.creation_date)
        return make_response(
            jsonify({'id': record.id,
                     'value': record.value,
                     'creation_time': serialized_date,
                     'counter_id': record.counter_id}), 201,
            {'Location': url_for('record', id=record.id, _external=True)})

    def get(self):
        args = parser.parse_args()
        id = args['id']
        counter_id = args['counter_id']
        email = args['email']
        if not CounterService.check_owner(counter_id, email):
            abort(403)  # User isn't the counter's owner
        record = RecordService.get_record_by_id(id)
        serialized_date = SerializeUtils.serialize_date(record.creation_date)
        if not record:
            abort(400)  # No counter
        return ({'id': record.id,
                 'value': record.value,
                 'counter_id': record.counter_id,
                 'creation_time': serialized_date})

    def post(self):
        args = parser.parse_args()
        id = args['id']
        value = args['value']
        counter_id = args['counter_id']
        email = args['email']
        if not CounterService.check_owner(counter_id, email):
            abort(403)  # User isn't the counter's owner
        if value is None or counter_id is None or email is None:
            abort(400)  # Missing arguments
        record = RecordService.update_record(id=id, value=value)
        return make_response(
            jsonify({'id': record.id,
                     'value': record.value,
                     'counter_id': record.counter_id}), 201,
            {'Location': url_for('record', id=record.id, _external=True)})

    def delete(self):
        args = parser.parse_args()
        id = args['id']
        counter_id = args['counter_id']
        email = args['email']
        if id is None or counter_id is None or email is None:
            abort(400)  # Missing arguments
        if not CounterService.check_owner(counter_id, email):
            abort(403)  # User isn't the counter's owner
        RecordService.delete_record(id)
        return jsonify({'status': 'deleted'})
