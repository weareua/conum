from random import randint
from flask import jsonify
from flask_restful import Resource
from flask_mail import Message
from conum import mail, parser, app
from conum.services import UserService


class ResetPasswordAPI(Resource):
    parser.add_argument('email')
    parser.add_argument('secret')
    parser.add_argument('password')

    def get(self, email):
        secret_number = str(randint(1000, 9999))
        # Save secret number hash in db
        UserService.hash_secret(email, secret_number)
        # Send email with reset secret number
        mail_url = app.config["SERVER_ADDRESS"] + "#Відновити пароль/" + email
        msg = Message("Conum",
                      recipients=[email])
        msg.html = "Якщо ви бажаєте скинути пароль, введіть це число <b>" + \
            secret_number + "</b> перейшовши за вказаним тицем: <a href='" +  \
            mail_url + "'>тиць</a>.</br>" + \
            "Керуйтеся підказками на сайті, аби отримати новий пароль."
        mail.send(msg)
        return jsonify({"status": "OK"})

    def put(self):
        '''Verify secret'''
        args = parser.parse_args()
        email = args['email']
        secret = args['secret']
        if email is None or secret is None:
            return jsonify({"status": "ERROR", "message": "missing arguments"})
        try:
            if UserService.verify_secret(email, secret):
                UserService.approve_secret(email)
                return jsonify({"status": "OK", "message": "secret approved"})
            else:
                return jsonify(
                    {"status": "ERROR", "message": "secret rejected"})
        except ValueError as ex:
            return jsonify({"status": "ERROR", "message": str(ex)})

    def post(self):
        '''Save new password'''
        args = parser.parse_args()
        email = args['email']
        password = args['password']
        if email is None or password is None:
            return jsonify({"status": "ERROR", "message": "missing arguments"})
        user = UserService.get_user_by_email(email)
        secret_status, message = UserService.secret_status(email)
        if message == "OLD" or secret_status is False:
            return jsonify({"status": "ERROR", "message": "old request"})
        else:
            UserService.hash_password(email, password)
            # Send updated notification email
            msg = Message("Conum",
                          recipients=[user.email])
            msg.html = "Ви успішно оновили пароль до обілкового запису " + \
                "в сервісі <b>Conum.</b></br>"
            mail.send(msg)

            return jsonify({"status": "OK", "updated": "true"})
        return jsonify({"status": "ERROR", "updated": "false"})
