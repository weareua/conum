from flask import jsonify
from flask_restful import Resource
from conum import auth
from conum.services import CounterService
from conum.utils import SerializeUtils


class CounterListAPI(Resource):
    decorators = [auth.login_required]

    counters = CounterService.get_all_counters()

    def get(self):
        serialized_counters = {}
        for counter in self.counters:
            serialized_counter = {}
            serialized_counter['records'] = {}
            serialized_counter['id'] = str(counter.id)
            serialized_counter['title'] = counter.title
            serialized_counter['dimension'] = counter.dimension
            serialized_counter['color'] = counter.color
            serialized_counter['creation_date'] = SerializeUtils.serialize_date(
                counter.creation_date)

            for record in counter.records:
                serialized_record = {}
                serialized_record['id'] = str(record.id)
                serialized_record['value'] = record.value
                serialized_record['creation_date'] = \
                    SerializeUtils.serialize_date(record.creation_date)
                serialized_counter[
                    "records"][str(record.id)] = serialized_record

            serialized_counters[str(counter.id)] = serialized_counter

        return jsonify({'counters': serialized_counters})
