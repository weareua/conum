from flask import jsonify
from flask_restful import Resource
from conum import auth, parser
from conum.services import UserService


class TokenAPI(Resource):
    decorators = [auth.login_required]

    parser.add_argument('email')

    def post(self):
        args = parser.parse_args()
        email = args['email']
        token = UserService.generate_auth_token(email, 600)
        return jsonify({'token': token.decode('ascii'), 'duration': 600})
