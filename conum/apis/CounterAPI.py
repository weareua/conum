from flask import jsonify, abort, url_for, make_response
from flask_restful import Resource
from conum import auth, parser
from conum.services import CounterService, UserService
from conum.utils import SerializeUtils


class CounterAPI(Resource):
    decorators = [auth.login_required]

    parser.add_argument('id')
    parser.add_argument('title')
    parser.add_argument('dimension')
    parser.add_argument('color')
    parser.add_argument('email')

    def put(self):
        args = parser.parse_args()
        title = args['title']
        dimension = args['dimension']
        color = args['color']
        email = args['email']
        if title is None or dimension is None or color is None or email is None:
            abort(400)  # Missing arguments
        user = UserService.get_user_by_email(email)
        counter = CounterService.create_counter(
            title, dimension, color, user)
        return make_response(
            jsonify({'id': counter.id,
                     'title': counter.title,
                     'dimension': counter.dimension,
                     'color': counter.color}), 201,
            {'Location': url_for('counter', id=counter.id, _external=True)})

    def get(self):
        args = parser.parse_args()
        id = args['id']
        email = args['email']
        if not CounterService.check_owner(id, email):
            abort(403)  # User isn't the counter's owner
        counter = CounterService.get_counter_by_id(id)
        if not counter:
            abort(400)  # No counter
        serialized_date = SerializeUtils.serialize_date(counter.creation_date)
        return ({'id': counter.id,
                 'title': counter.title,
                 'dimension': counter.dimension,
                 'color': counter.color,
                 'creation_date': serialized_date})

    def post(self):
        args = parser.parse_args()
        id = args['id']
        title = args['title']
        dimension = args['dimension']
        color = args['color']
        email = args['email']
        if not CounterService.check_owner(id, email):
            abort(403)  # User isn't the counter's owner
        counter = CounterService.update_counter(
            id=id, title=title, dimension=dimension, color=color)
        return make_response(
            jsonify({'id': counter.id,
                     'title': counter.title,
                     'dimension': counter.dimension,
                     'color': counter.color}), 201,
            {'Location': url_for('counter', id=counter.id, _external=True)})

    def delete(self):
        args = parser.parse_args()
        id = args['id']
        email = args['email']
        if not CounterService.check_owner(id, email):
            abort(403)  # User isn't the counter's owner
        CounterService.delete_counter(id)
        return jsonify({'status': 'deleted'})
