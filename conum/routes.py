from conum import app, api
from conum.views import IndexView
from conum.apis import (UserAPI, TokenAPI, CounterAPI, CounterListAPI,
                          RecordAPI, ResetPasswordAPI)

app.add_url_rule('/', view_func=IndexView.as_view('index'))

api.add_resource(
    UserAPI.UserAPI, '/api/user', '/api/user/<int:id>', endpoint='user')
api.add_resource(TokenAPI.TokenAPI, '/api/token', endpoint='token')
api.add_resource(
    CounterAPI.CounterAPI, '/api/counter', endpoint='counter')
api.add_resource(
    CounterListAPI.CounterListAPI, '/api/counters', endpoint='counters')
api.add_resource(
    RecordAPI.RecordAPI, '/api/record', '/api/record/<int:id>',
    endpoint='record')
api.add_resource(
    ResetPasswordAPI.ResetPasswordAPI, '/api/reset',
    '/api/reset/<string:email>', endpoint='reset')
