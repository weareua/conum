from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
from flask_restful import Api, reqparse
from flask_mail import Mail


app = Flask(__name__)
app.config['SECRET_KEY'] = 'Bright dogs run to the rainbow'


# Setup SqlAlchemy
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://admin:admin@localhost/conumdb'
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)


# Setup mail server
app.config['MAIL_SERVER'] = 'smtp.googlemail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USERNAME'] = 'weareua@gmail.com'
app.config['MAIL_PASSWORD'] = ''
app.config['MAIL_DEFAULT_SENDER'] = 'mail@conum.org.ua'


# How many records get per request
app.config['RECORDS_PER_PAGE'] = 500

# How many hours to keep active recover session
app.config['RECOVERY_HOURS_WINDOW'] = 24

# Server name
app.config['SERVER_ADDRESS'] = 'http://localhost:5000/'

# Setup Flask-Mail
mail = Mail(app)

# Setup Flask-Restful
api = Api(app)

# Setup Flask-HTTPAuth
auth = HTTPBasicAuth()

# Setup Flask-Restful parser
parser = reqparse.RequestParser()

from conum import apis, views, models, routes, utils, cli  # noqa
