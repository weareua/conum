

function AppModel() {
    var self = this;

    self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization",
                        "Basic " + btoa(localStorage.token + ":"));
                },
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
        return $.ajax(request);
    };

    // Get counters
    self.getCounters = function() {
        self.ajax(Config.countersURI, "GET", "").done(function(data) {
            for (var key in data.counters) {

                var value = data.counters[key];
                var records = ko.observableArray();
                var lastRecord = ko.observableArray();

                // Set records
                for (var recordKey in value.records) {
                    var recordValue = value.records[recordKey];
                    records.push({
                        id: ko.observable(recordValue.id),
                        value: ko.observable(recordValue.value),
                        creationDate: ko.observable(recordValue.creation_date),
                    })
                }
                records.reverse();

                // Set counters
                AppModel.counters.push({
                    id: ko.observable(value.id),
                    title: ko.observable(value.title),
                    dimension: ko.observable(value.dimension),
                    color: ko.observable(value.color),
                    creationDate: ko.observable(value.creation_date),
                    records: records,
                })
            }
        }).fail(function(jqXHR) {
            console.log("ERROR", jqXHR)
            Config.goToRoute("#Вийти");
        });
    };

    // Edit counter title
    self.changeCounterTitle = function(counterId, newTitle) {
        var data = {"id": counterId, "title": newTitle, "email": localStorage.email};
        self.ajax(Config.counterURI, "POST", data).done(function(data){
            // Change title
            $('#title-for-' + counterId).text(data.title);
        }).fail(function(){
            console.log("ERROR");
        })
    }

    // Edit counter dimension
    self.changeCounterDimension = function(counterId, newDimension) {
        var data = {"id": counterId, "dimension": newDimension, "email": localStorage.email};
        self.ajax(Config.counterURI, "POST", data).done(function(data){
            console.log(data);
            // Change dimension
            $('#dimension-for-' + counterId).text(data.dimension);
        }).fail(function(){
            console.log("ERROR");
        })
    }

};

AppModel.counters = ko.observableArray();
