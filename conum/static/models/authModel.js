

function AuthModel() {
    var self = this;

    self.formLogin = ko.observable(false);
    self.formRegister = ko.observable(false);
    self.formReset = ko.observable(false);
    self.formConfirmReset = ko.observable(false);
    self.formNewPassword = ko.observable(false);
    self.formResetSuccess = ko.observable(false);
    self.formSecretRejected = ko.observable(false);
    self.formRestartProcess = ko.observable(false);
    self.formFatalError = ko.observable(false);

    self.loginValidationGroup = ko.validation.group(Config.email, Config.password);
    self.registerValidationGroup = ko.validation.group(
            Config.email, Config.password, Config.confirmPassword);
    self.resetEmailValidationGroup = ko.validation.group(Config.email);
    self.checkSecretValidationGroup = ko.validation.group(Config.secret);
    self.newPasswordValidationGroup = ko.validation.group(
            Config.newPassword, Config.confirmNewPassword);

    self.ajax = function(uri, method, data, type) {
        switch (type) {
            case "token":
                var request = {
                    url: uri,
                    type: method,
                    contentType: "application/json",
                    accepts: "application/json",
                    cache: false,
                    dataType: 'json',
                    data: JSON.stringify(data),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization",
                            "Basic " + btoa(Config.email() + ":" + Config.password()));
                    },
                    error: function(jqXHR) {
                        console.log("ajax error " + jqXHR.status);
                    }
                };
                break;
            case "noCreds":
                var request = {
                    url: uri,
                    type: method,
                    contentType: "application/json",
                    accepts: "application/json",
                    cache: false,
                    dataType: 'json',
                    data: JSON.stringify(data),
                    error: function(jqXHR) {
                        console.log("ajax error " + jqXHR.status);
                    }
                };
                break;
        }
        return $.ajax(request);
    }

    // Validation
    self.isValid = function(validationGroup){
        if (validationGroup().length > 0){
            return false;
        }
        else {
            return true
        }
    }

    // Register
    self.register = function() {
        if (self.isValid(self.registerValidationGroup)){
            var data = {"email": Config.email(), "password": Config.password()};
            self.ajax(Config.userURI, "PUT", data, "noCreds").done(function(data) {
                self.login();
            }).fail(function(jqXHR) {
                console.log("ERROR", jqXHR);
            });
        }
	};

    // Login
    self.login = function() {
        if (self.isValid(self.loginValidationGroup)){

            // Save email for further use
            localStorage.setItem("email", Config.email());
            var data = {"email": Config.email()};

            self.ajax(Config.tokenURI, "POST", data, "token").done(function(data) {
                localStorage.setItem("token", data.token);
                Config.password(null);
                Config.goToRoute("Лічильники");
            }).fail(function(jqXHR) {
                console.log("ERROR", jqXHR);
                Config.goToRoute("#Фатальна Помилка");
            });
        }
	};

    // Send reset email (1/3)
    self.sendResetEmail = function() {
        if (self.isValid(self.resetEmailValidationGroup)){
            var resetURI = Config.resetURI + "/" + Config.email();
            console.log(resetURI);
            self.ajax(resetURI, "GET", "", "noCreds").done(function(data){
                console.log(data);
                // TODO: Notify User
                if (data.status === "OK"){
                    Config.goToRoute("#Відновити пароль/" + Config.email());
                }
                else {
                    self.resetAuthForms();
                    self.formRestartProcess(true);
                }
            }).fail(function(kqXHR){
                console.log("ERROR", jqXHR);
            });
        }
    }

    // Send reset request (2/3)
    self.resetRequest = function() {
        if (self.isValid(self.checkSecretValidationGroup)){
           var data = {"email": Config.email(), "secret": Config.secret()};
           console.log(data);
           self.ajax(Config.resetURI, "PUT", data, "noCreds").done(function(data){
                console.log(data);
                // TODO: Notify User
                self.resetAuthForms();
                if (data.status === "OK"){
                    self.formNewPassword(true);
                }
                else if(data.status === "ERROR"){
                    Config.secret(null);
                    if (data.message === "secret rejected" ||
                            data.message === "hash could not be identified"){
                        self.formSecretRejected(true);
                    }
                else {
                    Config.secret(null);
                    self.formRestartProcess(true);
                }
                }
           }).fail(function(kqXHR){
                console.log("ERROR", jqXHR);
           });
        }
    }

    // Set new password (3/3)
    self.newPassword = function() {
        if (self.isValid(self.newPasswordValidationGroup)){
           var data = {"email": Config.email(), "password": Config.newPassword()};
			console.log(data);
           self.ajax(Config.resetURI, "POST", data, "noCreds").done(function(data){
                console.log(data);
                // Notify User
                if (data.status === "OK"){
					Config.goToRoute("Пароль скинуто");
                }
                else {
                    self.resetAuthForms();
                    Config.secret(null);
                    self.formRestartProcess(true);
                }

           }).fail(function(kqXHR){
                console.log("ERROR", jqXHR);
           });
        }
    }

    // Reset all auth forms
    self.resetAuthForms = function(){
		self.formLogin(false);
		self.formReset(false);
		self.formRegister(false);
		self.formConfirmReset(false);
		self.formNewPassword(false);
		self.formResetSuccess(false);
		self.formSecretRejected(false);
		self.formRestartProcess(false);
		self.formFatalError(false);
    }
};


