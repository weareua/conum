(function(factory) {
	// Module systems magic dance.
	/*global require,ko.validation,define,module*/
	if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
		// CommonJS or Node
        module.exports = factory(require('../'));
	} else if (typeof define === 'function' && define['amd']) {
		// AMD anonymous module
		define(['knockout.validation'], factory);
	} else {
		// <script> tag: use the global `ko.validation` object
		factory(ko.validation);
	}
}(function(kv) {
	if (!kv || typeof kv.defineLocale !== 'function') {
		throw new Error('Knockout-Validation is required, please ensure it is loaded before this localization file');
	}
	return kv.defineLocale('ua-UA', {
		required: 'Це поле є обов`язковим',
		min: 'Введене число має бути більше чи дорівнювати {0}',
		max: 'Введене число має бути менше чи дорівнювати {0}',
		minLength: 'Введіть принаймні {0} символи',
		maxLength: 'Кількість символів не має перевищувати {0}',
		pattern: 'Перевірте це поле',
		step: 'Значення має бути кратним {0}',
		email: 'Вкажіть адресу електронної пошти',
		date: 'Введіть правильну дату',
		dateISO: 'Введіть правильну дату в форматі ISO',
		number: 'Введіть число',
		digit: 'Введіть цифри',
		phoneUS: 'Вкажіть правильний телефонний номер',
		equal: 'Ці значення мають бути однаковими',
		notEqual: 'Оберіть інше значення',
		unique: 'Вкажіть унікальне значення'
	});
}));
