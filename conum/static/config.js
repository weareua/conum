

function Config() {
}

// Credentials
Config.token = ko.observable();
Config.secret = ko.observable().extend({ required: true, digits: true, minLength: 4,  maxLength: 4});
Config.email = ko.observable().extend({ required: true, email: true});
Config.password = ko.observable().extend({ required: true, minLength: 6});
Config.confirmPassword = ko.observable().extend({ required: true, equal: Config.password});
Config.newPassword = ko.observable().extend({ required: true, minLength: 6});
Config.confirmNewPassword = ko.observable().extend({ required: true, equal: Config.newPassword});

//URLs
var rootAddress = "http://localhost:5000/";
Config.userURI = rootAddress + 'api/user';
Config.tokenURI = rootAddress + 'api/token';
Config.resetURI = rootAddress + 'api/reset';
Config.counterURI = rootAddress + 'api/counter';
Config.countersURI = rootAddress + 'api/counters';

Config.goToRoute = function(route) { location.hash = route };
