

function Routes() {
    var self = this;

    self.auth = new AuthModel();
    self.app = new AppModel();

    self.externalRoutes = ['Ввійти', 'Зареєструватися', 'Відновити пароль'];
    self.internalRoutes = ['Вийти'];

    self.currentRoutes = ko.observableArray();
    self.chosenRouteId = ko.observable();

    // Logout -> clear credentials
    self.logout = function(){
        Config.email(null);
        Config.password(null);
        Config.confirmPassword(null);
        Config.newPassword(null);
        Config.confirmNewPassword(null);
        Config.secret(null);
        AppModel.counters(null);
        localStorage.removeItem("localToken");
    }


    // Client-side routes
    Sammy(function() {
        this.get('#Ввійти', function() {
            self.currentRoutes(self.externalRoutes);

            $('form').fadeOut("fast", function(){
				self.auth.resetAuthForms()
				self.auth.formLogin(true);
			});

            // if there are no forms
            if (typeof document.forms[0] === 'undefined') {
				self.auth.formLogin(true);
            }
        });

        this.get('#Зареєструватися', function() {
            $('form').fadeOut("fast", function(){
				self.auth.resetAuthForms()
				self.auth.formRegister(true);
			});
        });

        this.get('#Відновити пароль', function() {
            $('form').fadeOut("fast", function(){
                self.auth.resetAuthForms();
                self.auth.formReset(true);
			});
        });

        this.get('#Відновити пароль/:email', function() {
            $('form').fadeOut("fast", function(){
                Config.email(this.params['email']);

                self.auth.resetAuthForms();
                self.auth.formConfirmReset(true);
			});
        });

        this.get('#%D0%92%D1%96%D0%B4%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D0%B8%20%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D1%8C/:email', function() {
            $('form').fadeOut("fast", function(){
                Config.email(this.params['email']);

                self.auth.resetAuthForms();
                self.auth.formConfirmReset(true);
			});
        });

        this.get('#Пароль скинуто', function() {
            $('form').fadeOut("fast", function(){
                self.auth.resetAuthForms();
                self.auth.formResetSuccess(true);
			});
        });

        this.get('#Фатальна Помилка', function() {
            $('form').fadeOut("fast", function(){
                self.auth.resetAuthForms();
                self.auth.formFatalError(true);
			});
        });

        this.get('#Лічильники', function() {
            self.currentRoutes(self.internalRoutes);
            $('#Лічильники').css("background-color", "#e95420");
            self.auth.resetAuthForms();
            self.app.getCounters();
        });

        this.get('#Профіль', function() {
        });

        this.get('#Вийти', function() {
            self.logout();
            Config.goToRoute('');
        });

        this.get('', function() {
            this.app.runRoute('get', '#Ввійти')
            $('nav li').css('background-color', '');
            $('form span.validationMessage').empty();
            $('#Ввійти').css("background-color", "#e95420");
        });

    }).run();
};

