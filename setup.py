from setuptools import setup

setup(
    name='conum',
    packages=['conum'],
    include_package_data=True,
    install_requires=[
        'conum',
    ],
)
